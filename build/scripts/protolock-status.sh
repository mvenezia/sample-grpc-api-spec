#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

THIS_DIRECTORY=$(dirname "${BASH_SOURCE}")
PROJECT_DIRECTORY=${PWD}/${THIS_DIRECTORY}/../..

# Version v1 stuff
VERSION_V1_PROTOBUF_DIRECTORY_API=${PROJECT_DIRECTORY}/api/version/v1

# Math v1 stuff
MATH_V1_PROTOBUF_DIRECTORY_API=${PROJECT_DIRECTORY}/api/math/v1

DOCKER_ARGUMENTS="run --rm=true -it -w /code"
DOCKER_IMAGE=quay.io/venezia/protolock:v0.15.2

echo
echo "having protolock commit changes..."
echo "running docker ${DOCKER_ARGUMENTS} -v ${VERSION_V1_PROTOBUF_DIRECTORY_API}:/code ${DOCKER_IMAGE} status"
docker ${DOCKER_ARGUMENTS} -v ${VERSION_V1_PROTOBUF_DIRECTORY_API}:/code ${DOCKER_IMAGE} status
echo "running docker ${DOCKER_ARGUMENTS} -v ${MATH_V1_PROTOBUF_DIRECTORY_API}:/code ${DOCKER_IMAGE} status"
docker ${DOCKER_ARGUMENTS} -v ${MATH_V1_PROTOBUF_DIRECTORY_API}:/code ${DOCKER_IMAGE} status