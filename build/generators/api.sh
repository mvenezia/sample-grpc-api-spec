#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

THIS_DIRECTORY=$(dirname "${BASH_SOURCE}")

PROJECT_DIRECTORY=${THIS_DIRECTORY}/../..
THIRD_PARTY_LIBRARY_API=${PROJECT_DIRECTORY}/third_party
GRPC_GATEWAY_LIBRARY_API=${PROJECT_DIRECTORY}/vendor/github.com/grpc-ecosystem/grpc-gateway

# Version v1 stuff
VERSION_V1_PROTOBUF_DIRECTORY_API=${PROJECT_DIRECTORY}/api/version/v1
VERSION_V1_PROTOBUF_FILE_API=${VERSION_V1_PROTOBUF_DIRECTORY_API}/versionv1.proto
VERSION_V1_DESTINATION_LIBRARY_API=pkg/generated/api/version/v1
VERSION_V1_DESTINATION_LOCATION_API=${PROJECT_DIRECTORY}/${VERSION_V1_DESTINATION_LIBRARY_API}
VERSION_V1_SWAGGER_DESTINATION=${PROJECT_DIRECTORY}/assets/generated/swagger/version/v1
VERSION_V1_DOCS_DESTINATION=${PROJECT_DIRECTORY}/docs/api-generated/version/v1

# Math v1 stuff
MATH_V1_PROTOBUF_DIRECTORY_API=${PROJECT_DIRECTORY}/api/math/v1
MATH_V1_PROTOBUF_FILE_API=${MATH_V1_PROTOBUF_DIRECTORY_API}/mathv1.proto
MATH_V1_DESTINATION_LIBRARY_API=pkg/generated/api/math/v1
MATH_V1_DESTINATION_LOCATION_API=${PROJECT_DIRECTORY}/${MATH_V1_DESTINATION_LIBRARY_API}
MATH_V1_SWAGGER_DESTINATION=${PROJECT_DIRECTORY}/assets/generated/swagger/math/v1
MATH_V1_DOCS_DESTINATION=${PROJECT_DIRECTORY}/docs/api-generated/math/v1

DOCKER_ARGUMENTS="run --rm=true -it -v ${PWD}:/code -w /code --entrypoint /usr/bin/protoc"
DOCKER_IMAGE=quay.io/venezia/grpc-golang:v3.14

#echo "Ensuring Destination Directory ( ${VERSION_V1_DESTINATION_LOCATION_API} ) Exists..."
#mkdir -p ${VERSION_V1_DESTINATION_LOCATION_API}

echo "Ensuring Destination Directory ( ${VERSION_V1_SWAGGER_DESTINATION} ) Exists..."
mkdir -p ${VERSION_V1_SWAGGER_DESTINATION}
echo "Ensuring Destination Directory ( ${MATH_V1_SWAGGER_DESTINATION} ) Exists..."
mkdir -p ${MATH_V1_SWAGGER_DESTINATION}

echo "Ensuring Destination Directory ( ${VERSION_V1_DOCS_DESTINATION} ) Exists..."
mkdir -p ${VERSION_V1_DOCS_DESTINATION}
echo "Ensuring Destination Directory ( ${MATH_V1_DOCS_DESTINATION} ) Exists..."
mkdir -p ${MATH_V1_DOCS_DESTINATION}

echo
echo "generating version v1 swagger docs..."
echo "protoc ${VERSION_V1_PROTOBUF_FILE_API} -I ${VERSION_V1_PROTOBUF_DIRECTORY_API}/ -I ${THIRD_PARTY_LIBRARY_API} --swagger_out=logtostderr=true,allow_merge=true,merge_file_name=api:${VERSION_V1_SWAGGER_DESTINATION}"
docker $DOCKER_ARGUMENTS $DOCKER_IMAGE ${VERSION_V1_PROTOBUF_FILE_API} -I ${VERSION_V1_PROTOBUF_DIRECTORY_API}/ -I ${THIRD_PARTY_LIBRARY_API} --openapiv2_out=logtostderr=true,allow_merge=true,merge_file_name=api:${VERSION_V1_SWAGGER_DESTINATION}
echo "generating math v1 swagger docs..."
echo "protoc ${MATH_V1_PROTOBUF_FILE_API} -I ${MATH_V1_PROTOBUF_DIRECTORY_API}/ -I ${THIRD_PARTY_LIBRARY_API} --swagger_out=logtostderr=true,allow_merge=true,merge_file_name=api:${MATH_V1_SWAGGER_DESTINATION}"
docker $DOCKER_ARGUMENTS $DOCKER_IMAGE ${MATH_V1_PROTOBUF_FILE_API} -I ${MATH_V1_PROTOBUF_DIRECTORY_API}/ -I ${THIRD_PARTY_LIBRARY_API} --openapiv2_out=logtostderr=true,allow_merge=true,merge_file_name=api:${MATH_V1_SWAGGER_DESTINATION}

echo
echo "generating version v1 api docs..."
echo "protoc ${VERSION_V1_PROTOBUF_FILE_API} -I ${VERSION_V1_PROTOBUF_DIRECTORY_API}/ -I ${THIRD_PARTY_LIBRARY_API} --doc_out ${VERSION_V1_DOCS_DESTINATION} --doc_opt=markdown,api.md"
docker $DOCKER_ARGUMENTS $DOCKER_IMAGE ${VERSION_V1_PROTOBUF_FILE_API} -I ${VERSION_V1_PROTOBUF_DIRECTORY_API}/ -I ${THIRD_PARTY_LIBRARY_API} --doc_out ${VERSION_V1_DOCS_DESTINATION} --doc_opt=markdown,api.md
echo "generating math v1 api docs..."
echo "protoc ${MATH_V1_PROTOBUF_FILE_API} -I ${MATH_V1_PROTOBUF_DIRECTORY_API}/ -I ${THIRD_PARTY_LIBRARY_API} --doc_out ${MATH_V1_DOCS_DESTINATION} --doc_opt=markdown,api.md"
docker $DOCKER_ARGUMENTS $DOCKER_IMAGE ${MATH_V1_PROTOBUF_FILE_API} -I ${MATH_V1_PROTOBUF_DIRECTORY_API}/ -I ${THIRD_PARTY_LIBRARY_API} --doc_out ${MATH_V1_DOCS_DESTINATION} --doc_opt=markdown,api.md
