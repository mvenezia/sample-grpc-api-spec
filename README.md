# Sample gRPC API Specification

## Important file locations

* protobuf files are located within [api/](api/)
* swagger.json files are located within [assets/generated/swagger/](assets/generated/swagger/)
* Makefile is located at [build/Makefile](build/Makefile)
* Generators are located in [build/generators](build/generators)

## Record of Truth

The record of truth for the API is [api/](api/)

## Generating swagger file

To regenerate swagger/documentation, please do the following:

```bash
make -f build/Makefile generators
```

Note: you need to have `docker` in your path.

## What is a `.proto` file?

The best source of knowledge on `.proto` files is [Google's Documentation](https://developers.google.com/protocol-buffers/)

## What is protolock?

For the best explanation see [protolock's homepage](https://protolock.dev/)

In short, an attempt to look to see if the current api spec has broken compatibility in some sense (there are a number of rules)

## How do I check and update protolock's saved data?
As of right now, the best way may be to update protolock's saved data yourself, presumably without the `--force` option

* An example of checking your protolock status on your local environment would be:
```
make -f ./build/Makefile protolock-status
```
* To update protolock with the new data, an example command would be:
```
make -f ./build/Makefile protolock-commit
```
